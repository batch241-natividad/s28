db.getCollection('rooms').insertOne(
	{
	Name: "single",
	Accomodates: 2,
	Price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false
})

db.getCollection('rooms').insertMany([

{
	Name: "double",
	Accomodates: 3,
	Price: 2000,
	description: " room fit for a small family going ona vacation",
	roomsAvailable: 5,
	isAvailable: false
},
{
	Name: "queen",
	Accomodates: 4,
	Price: 4000,
	description: "A room with a queen sized bed perform for a simple getaway",
	roomsAvailable: 15,
	isAvailable: false}])



db.getCollection('rooms').find({Name: "double"})

db.getCollection('rooms').updateOne({Name: "queen"},
{
	$set:{

		roomsAvailable:0
	}

})

db.getCollection('rooms').deleteMany({
	roomsAvailable:0
})

db.getCollection('rooms').finds({});