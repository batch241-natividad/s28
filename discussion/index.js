//CRUD Operations

/*
-CRUD operations are the heart of any backend applications
-CRUD Stands for Create,read,update, and delete
-MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods

CREATE: Inserting documents

// INSERT ONE

Syntax:
	db.collectionName.insertOne({})

Inserting/Assigning values in JS objects:

	object.object.method({object})

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234567",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department:"none"
})

//INSERT MANY

/*
Syntax:
db.users.insertmany([
	{objectA},
	{objectB},
])

*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "342516",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}



	])

// READ: FINDING DOCUMENTS

// FIND ALL

/*
Syntax:
	db.collectionName.find();

*/

db.users.find();

// Finding users with single arguments
/*
	Syntax:
		db.collectionName.find({ field: value })

*/

db.users.find({firstName: "Stephen"});

//Finding users with multiple arguments
//no match
db.users.find({firstName:"Stephen", age: 20})


//with match
db.users.find({firstName:"Stephen", age: 76, lastName:"Hawking"})

//UPDATE: Updating documents
//Repeat: Jane to be updated

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234567",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department:"none"
});


// UPDATE ONE

/*
	Synbtax:
		db.collectionName.updateOne({criteria} , {$set: (field: value)})
*/

db.users.updateOne( 
	{firstName: "Jane"},
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "000000",
				email: "janegates@gmail.com"

			},
			courses: ["AWS", "Google cloud", "Azure"],
			department: "infrastructure",
			status:"Active"
		}
	}
)

db.users.find({firstName: "Jane"});

// update many

db.users.updateMany(
	{department: "none"	},
	{
		$set: {
			department: "HR"
		}
	}
	)

db.users.find().pretty();

//REPLACE ONE
/*
	- Can be used if replacing the whole document is necessary
	- Syntax:
		db.collectionName.replaceOne({criteria}, {$set: {field: value}})


*/

db.users.replaceOne(
	{lastName:"Gates"},
        {
			firstName:"Bill",
			lastName:"Clinton"
		}
	

	)
db.users.find({firstName: "Bill"});

//DELETE: DELETING DOCUMENTS


db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "000000"
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

db.users.find({firstName: "test"});

//Deleting a single document

/*
Syntax:
	db.collectionName.deleteOne({criteria})

*/

db.users.deleteOne({firstName:"Test"});
db.users.deleteOne({firstName: "Test"});


db.users.deleteOne({age: 76});
db.users.find();


//Delete Many
db.users.deleteMany({
	courses: []
})
db.users.find();

//Delete all
//db.users.delete()
/*
-Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents

-DO NOT USE: db.collectionName.deleteMany()
-Syntax:
	db.collectionName.deleteMany({criteria})

*/


//Advanced queries

//Query an embedded document

db.users.find({
	contact: {
		phone: "1234567",
		email: "janedoe@gmail.com"
	}

})

//Find the document with the email "janedoe@gmail.com"
//Querying on nested fields

db.users.find({"contact.email" :"janedoe@gmail.com" });

//Querying an Array with exact elements

db.users.find({courses: ["React","Laravel","SASS"]})

//Quering an element with regard to order

db.users.find({courses:{$all: ["React","Laravel","SASS"]}})

// Make an array to query

db.users.insert